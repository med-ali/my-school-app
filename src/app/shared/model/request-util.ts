import { HttpParams } from '@angular/common/http';
import { URLSearchParams } from '@angular/http';

export const createRequestOption = (req?: any): HttpParams => {
    let Params = new HttpParams();
    if (req) {
        for(let key in req){
            let value = req[key];
            if(key != 'sort' && value!=null){
                Params = Params.append(key, value);
            }else if(key== 'sort'){
                for (let s of value) {
                    Params = Params.append('sort', s);
                }
            }
        }
       
    }
    return Params;
};
