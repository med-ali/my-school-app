import { Component, OnInit } from '@angular/core';
import { Message } from '../../models/toast.interface';
import { NgbActiveModal,NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  message: Message = null;
  constructor(private activeModal: NgbActiveModal){ }

  show(msg: Message ){
    debugger;
    this.message = msg;
    setTimeout(()=>{
      this.message = null;
    }, 500000)
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }
  ngOnInit() { 
    //this.show(this.message)
    console.log(this.message)
  }

}
