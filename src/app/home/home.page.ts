import { Component,ViewChild } from '@angular/core';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/user';
import { Exam } from '../models/Exam.model';
import { RankEtudiant } from '../models/RankEtudiant.model';
import { Clas } from '../models/class.model';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { FormControl } from '@angular/forms';
import { Subject } from '../models/subject.model';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { IonicSelectableComponent } from 'ionic-selectable';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  user: User
  userid : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  isAdmin:boolean;
  type:string;
  subjects:Subject[];
  selectedSubject:Subject;
  clas:string;
  notes:EtudiantSubjectNote[];
  eventSubscriber: Subscription;
  dateSelected:any;
  phone:string;
  address:string;
  classes:Clas[]
  dateSelectedMonth:any;
  moyByMonth:number;
  isMonth:boolean;
  rank:number;
  selectedClass:Clas;
  ranksEtudiants:RankEtudiant[];
   // pager object
   pager: any = {};

   // paged items
   pagedItems: any[];
   // array of all items to be paged
   private allItems: any[];
   @ViewChild('selectComponent') selectComponent: IonicSelectableComponent;
   constructor(private subjectService: SubjectService,
    private userService: UserService,private router: Router,
    private modalService: NgbModal,
    private authService: AuthService,
    private route: ActivatedRoute) { }
    ngOnInit() {
      this.ranksEtudiants = [];
    }
    ionViewWillEnter() {
      /*this.authService.user().subscribe(
        user => {
          this.user = user;
        }
      );*/
      this.ranksEtudiants = [];
      this.authService.getUser().subscribe(
        user => {
           user.then(data => {
             this.user = data;
             this.find();
             console.log("data"+data["id"])
           });
        }
      );
    }
    find(){
    this.userService.findByid(this.user["id"]).subscribe( (value) => {
      this.user =  value["body"];
      this.userLastName = value["body"]["lastName"]
      this.userFirstName = value["body"]["firstName"]
      this.userEmail = value["body"]["email"]
      this.type = value["body"]["type"]
      this.userRole = value["body"]["roles"][0]["name"]   
      this.phone = value["body"]["phone"]
      this.address = value["body"]["address"]
      this.selectedClass = value["body"]["clas"]
      if (this.type == "etudiant" && value["body"]["clas"]!=null){
        this.clas = value["body"]["clas"]["name"]
       this.subjectService.findSubjectsByEudiantId(this.user["id"],0).subscribe( (value) => {
         if (value["body"]) {
           this.subjects = value["body"]["content"];
         }
      })
      }
   })
  }

  onOptionsSelected(){ 
    this.isMonth = false ;
    console.log(this.selectedSubject+"111subjects"+this.selectedSubject.title);
  }

  onDateSelectedMonth(){
    this.isMonth = true;
    this.queryByMonth(0,this.dateSelectedMonth);
  }

  queryByMonth(page:number,date:any){
    this.subjectService.findSubjectRankByMonth(this.selectedClass.id,this.selectedSubject.id,date,page).subscribe( (value) => {
      this.ranksEtudiants = value["body"]["content"];
      this.allItems = value["body"]["content"];
      //this.setPageS(1);
      this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"] 
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }   
      }) 
  }

  loadData(event?:any) {
    this.currentPage = this.ranksEtudiants.length % 10;
    console.log(this.totalPages+"fefefe"+this.currentPage)
    setTimeout(() => {
      this.subjectService.findSubjectRankByMonth(this.selectedClass.id,this.selectedSubject.id,this.dateSelectedMonth,this.currentPage-1).subscribe(  
        (value) => { 
          this.ranksEtudiants.concat(value["body"]["content"]); 
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.currentPage -1 === this.totalPages) {
            //infiniteScrollEvent.enable(false);
            event.target.complete();
            // Disable the infinite scroll
            event.target.disabled = true;
          }
      }
    );
    }, 10);
  }
}
