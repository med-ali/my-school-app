import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfBySubjectPage } from './prof-by-subject.page';

describe('ProfBySubjectPage', () => {
  let component: ProfBySubjectPage;
  let fixture: ComponentFixture<ProfBySubjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfBySubjectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfBySubjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
