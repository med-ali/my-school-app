import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfBySubjectPage } from './prof-by-subject.page';

const routes: Routes = [
  {
    path: '',
    component: ProfBySubjectPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfBySubjectPage]
})
export class ProfBySubjectPageModule {}
