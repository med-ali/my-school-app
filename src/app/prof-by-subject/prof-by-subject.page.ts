import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../services/subject.service';
import { UserService } from '../services/user.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/user';
import { Subject } from '../models/subject.model';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-prof-by-subject',
  templateUrl: './prof-by-subject.page.html',
  styleUrls: ['./prof-by-subject.page.scss'],
})
export class ProfBySubjectPage implements OnInit {
  user: User
  userid : number
  classId : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  subject: Subject
  className : string
  classDescription:String
  totalItems: any;
  queryCount: any;
  page: any;
  users:User[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  usersClass : User[]
  subscription: Subscription;
  message: any;
  title:string;
  description:string;
  profNumber:number;
  constructor(
    private subjectService: SubjectService,
    private userService: UserService,private router: Router,private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getAll()
    this.subjectService.find(this.route["snapshot"]["params"]["id"]).subscribe( (value) => {
      this.subject = value;
      this.title = value["title"]
      this.description = value["description"]
      this.profNumber = value["profNumber"]
      this.userRole = localStorage.getItem('userRole')
   })
  }
  getAll(){
    this.currentPage = 0
    this.userService.findProfBySubjectId(this.route["snapshot"]["params"]["id"],0).subscribe( (value) => { 
      this.users = value["body"]["content"]
      this.usersClass = value["body"]["content"]
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"]
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      }
    }) 
  }

  loadData(event?:any) {
    this.currentPage = this.users.length % 10;
    console.log(this.totalPages+"fefefe"+this.currentPage)
    setTimeout(() => {
      this.userService.findProfBySubjectId(this.route["snapshot"]["params"]["id"],this.currentPage-1).subscribe(  
        (value) => { 
          this.users.concat(value["body"]["content"]); 
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.currentPage === this.totalPages) {
            //infiniteScrollEvent.enable(false);
            event.target.complete();

            // Disable the infinite scroll
            event.target.disabled = true;
          }
      }
    );
    }, 10);
  }
}
