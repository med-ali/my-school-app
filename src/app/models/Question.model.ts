import { Exam } from './Exam.model';
export class Question {

    constructor(
      public title: string,
      public description: string,
      public response:string,
      public type:string,
      public score:number,
      public isValid:boolean 
    ) {}
  }