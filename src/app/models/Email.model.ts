import { User } from './user';
import { MMessage } from './Message.model';
export class Email {
    constructor(
      public msg: MMessage,
      public userListId: number[]
    ) {}
  }