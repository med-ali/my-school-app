import { User } from './user';
export class MMessage {
   
    constructor(
      public id: number,
      public message: string,
      public sender: User,
      public created_at:any,
      public updated_at:any,
    ) {}
  }