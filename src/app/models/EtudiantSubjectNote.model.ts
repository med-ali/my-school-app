import { User } from './user';
import { Subject } from './subject.model';
export class EtudiantSubjectNote {
   
    constructor(
      public id: number,
      public etudiant: User,
      public subject: Subject,
      public note:string,
      public score:number,
      public createdAt:any,
      public updatedAt:any,
      public fromProf:boolean
    ) {}
  }