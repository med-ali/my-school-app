
export class Clas{
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public etudiantNumber?: number,
        public deleted?:boolean
    ) {
    }
}
