import { User } from './user';
export class Subject {
   
    constructor(
      public id:number,
      public title: string,
      public description: string
    ) {}
  }