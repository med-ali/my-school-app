import { User } from './user';
export class Exam {
   
    constructor(
      public title: string,
      public description: string
    ) {}
  }