import { User } from '../models/user';
import { Injectable } from '@angular/core';
import { Subscription,Subject } from 'rxjs';
import { Router } from '@angular/router'; 
import { Observable } from 'rxjs';
import 'rxjs';
import { SERVER_API_URL } from '../app.constants';

import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class SearchService {
  UrlBase:string = SERVER_API_URL+'api';
 
  querySearch(queryString: string,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/users/search/?name='+queryString+'&page='+page, { observe: 'response' })     
  }
  
  constructor(private httpClient: HttpClient,private router: Router) { }
  findAllByName(queryString: string,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/users/search/?name='+queryString+'&page='+page, { observe: 'response' })     
  }
}