import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from '../app.constants';
import { ClassUser } from '../models/ClassUser.model';
import { Clas } from '../models/class.model';
import { Idea } from '../models/idea.model';
import { createRequestOption } from '../shared/model/request-util';
//import { ArticleNormalisation } from './article-normalisation.model';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
export type EntityResponseType = HttpResponse<Clas>;
@Injectable({
    providedIn: 'root'
  })
export class IdeaService {

    private resourceUrl = SERVER_API_URL + 'api/idea';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/idea';

    constructor(private http: HttpClient) { }

    create(idea: Idea): Observable<Idea> {
        //this.convertDate(article);
        return this.http.post<Idea>(this.resourceUrl, idea);
    }

    update(idea: Idea): Observable<Idea> {
        //this.convertDate(article);
        return this.http.put<Idea>(this.resourceUrl, idea);
    }

    find(id: number): Observable<Idea> {
        return this.http.get<Idea>(`${this.resourceUrl}/${id}`);
    }

    query(page:number,req?: any): Observable<HttpResponse<Idea[]>> {
        const options = createRequestOption(req);
        return this.http.get<Idea[]>(this.resourceUrl+'/page?page='+page, { params: options, observe: 'response' });
    }
    
    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
     
   
    search(req?: any): Observable<HttpResponse<Idea[]>> {
        const options = this.createAdvancedSearchOption(req);
        return this.http.get<Idea[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    createAdvancedSearchOption = (req?: any): HttpParams => {
        let Params = new HttpParams();
        if (req) {
            // Begin assigning parameters
            if (req.page) {
                Params = Params.append('page', req.page)
            }
            if (req.size) {
                Params = Params.append('size', req.size)
            }
            if (req.query) {
                Params = Params.append('q', req.query)
            }
            if (req.filter) {
                Params = Params.append('filter', req.filter)
            }
            if (req.sort) {
                for (const s of req.sort) {
                    Params = Params.append('sort', s);
                }
            }
            if (req.supplier) {
                Params = Params.append('supplier', req.supplier)
            }
            if (req.qty) {
                Params = Params.append('qty', req.qty)
            }

        }
        return Params;
    };
   
    updateVote(idea: Idea,idUser:any): Observable<Idea> {
        //this.convertDate(article);
        return this.http.put<Idea>(this.resourceUrl+"/vote/"+idUser, idea);
    }

    findIdeaByUser(userId:any,req?: any): Observable<HttpResponse<Idea[]>> {
        const options = createRequestOption(req);
        return this.http.get<Idea[]>(this.resourceUrl+'/user/'+userId, { params: options, observe: 'response' });
    }

    findIdeaByDate(date:any,page:number): Observable<HttpResponse<any[]>>{
        return this.http.get<Idea[]>(this.resourceUrl+'/date/?date='+date+'&page='+page , {  observe: 'response' })
    }
}
