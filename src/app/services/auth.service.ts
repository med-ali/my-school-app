import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
import { User } from '../models/user';
import { Storage } from '@ionic/storage';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token:any;
  user:any;
  constructor(
    private http: HttpClient,
    //private storage: NativeStorage,
    private env: EnvService,
    private storage: Storage
  ) { }
  login(email: String, password: String) { 
    return this.http.post(this.env.API_URL + 'auth/signin',
      {email: email, password: password}
    ).pipe(
      tap(data => {
        console.log(data["jwtResponse"]["accessToken"]+'Token 222222 Stored'+data["user"]["id"]);
        this.storage.set('token', data["jwtResponse"]["accessToken"])
        this.storage.set('user', data["user"])
        .then(
          () => {
            console.log('2Token Stored//'+data["jwtResponse"]["accessToken"]);
          },
          error => console.error('Error storing item', error)
        );
        this.token = data["jwtResponse"]["accessToken"];
        this.isLoggedIn = true;
        return data["jwtResponse"]["accessToken"];
      }),
    );
  }
  register(fName: String, lName: String, email: String, password: String) {
    return this.http.post(this.env.API_URL + 'auth/register',
      {fName: fName, lName: lName, email: email, password: password}
    )
  }
  /*logout() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["access_token"]
    });
    return this.http.get(this.env.API_URL + 'auth/logout', { headers: headers })
    .pipe(
      tap(data => {
        this.storage.remove("token");
        this.isLoggedIn = false;
        delete this.token;
        return data;
      })
    )
  }*/
  logout(){
    this.storage.remove("token");
    this.isLoggedIn = false;
    delete this.token;
  }
  /*user() {
    const headers = new HttpHeaders({
      'Authorization': this.token["token_type"]+" "+this.token["access_token"]
    });
    return this.http.get<User>(this.env.API_URL + 'auth/user', { headers: headers })
    .pipe(
      tap(user => {
        return user;
      })
    )
  }*/
  getToken() {
    return this.storage.get('token').then(
      data => {
        this.token = data;
        if(this.token != null) {
          this.isLoggedIn=true;
        } else {
          this.isLoggedIn=false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn=false;
      }
    );
  }
  isUser() {
    return this.storage.get('user').then(
      data => {
         
        this.user = data;
        if(this.user != null) {
          this.isLoggedIn=true;
        } else {
          this.isLoggedIn=false;
        }
        return data;
      },
      error => {
        this.user = null;
        this.isLoggedIn=false;
      }
    );
  }

  public getUser(): any {
    console.log( 'Token 22222211111 Stored');
    return  new Observable(observer => {
          observer.next(this.storage.get('user'));
    }); 
}

}