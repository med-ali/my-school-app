import { User } from '../models/user';
import { Injectable,ViewChild } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams , HttpHeaders } from '@angular/common/http';
import { Subscription,Subject ,Observable} from 'rxjs';
import { Router } from '@angular/router';
import { Message } from '../models/toast.interface';
import { SERVER_API_URL } from '../app.constants';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export type EntityResponseType = HttpResponse<User>;
@Injectable({
  providedIn: 'root'
})
export class UserService {
  UrlBase:string = SERVER_API_URL+'api';
  
  /******************* *******************/
  findByid(id: any): Observable<EntityResponseType> {
    return this.httpClient.get<User>(this.UrlBase+'/users/userId/'+ id, { observe: 'response' })     
  }
  delete(id: number): Observable<HttpResponse<any>> {
    return this.httpClient.delete<any>(this.UrlBase+'/users/'+ id, { observe: 'response' });
  }
  UpdateUser(id: number,user:User) : Observable<EntityResponseType> {  
    return this.httpClient.put<User>(this.UrlBase+'/users/'+ id ,user, { observe: 'response' })     
  }
  create(user: User): Observable<EntityResponseType> {
    console.log(user)
    return this.httpClient.post<User>(this.UrlBase+'/auth/signup', user, { observe: 'response' });
  }
  findAll(): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/users', { observe: 'response' })     
  }
  query( page:number): Observable<HttpResponse<User[]>> { 
    return this.httpClient.get<User[]>(this.UrlBase+'/users/page?page='+page, {  observe: 'response' })
  }
  sortAsc(key:string, page:number): Observable<HttpResponse<User[]>> { 
    return this.httpClient.get<User[]>(this.UrlBase+'/users/sort/asc?key='+key+'&page='+page, {  observe: 'response' })
  }
  sortDesc(key:string, page:number): Observable<HttpResponse<User[]>> { 
    return this.httpClient.get<User[]>(this.UrlBase+'/users/sort/desc?key='+key+'&page='+page, {  observe: 'response' })
  }
  sortAscSearch(lastName:String,key:string, page:number): Observable<HttpResponse<User[]>> { 
    return this.httpClient.get<User[]>(this.UrlBase+'/users/sort/search/asc?name='+lastName+'&key='+key+'&page='+page, {  observe: 'response' })
  }
  sortDescSearch(lastName:String,key:string, page:number): Observable<HttpResponse<User[]>> { 
    return this.httpClient.get<User[]>(this.UrlBase+'/users/sort/search/desc?name='+lastName+'&key='+key+'&page='+page, {  observe: 'response' })
  }
  getAllUsersNoInscrit(id:number,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/students/usersnoinscrit/'+id+'/page?page='+page, { observe: 'response' })     
  }
  getUsersSearchByLastNameNoInscrit(id:number,page:number,lastName:String): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/exams/usersnoinscrit/'+id+'/search/page?page='+page+'&'+'name='+lastName, { observe: 'response' })     
  }
  findStudentByClassId(id: number,page:number): Observable<EntityResponseType> {
    return this.httpClient.get<User>(this.UrlBase+'/class/students/'+ id + '/page/?page='+page, { observe: 'response' })     
  }
  getAllUsersNoInscritByClass(id:number,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/students/notinscri/class/'+id+'/page?page='+page, { observe: 'response' })     
  }
  getAllProfsNoInscritByClass(id:number,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/profs/notinscri/class/'+id+'/page?page='+page, { observe: 'response' })     
  }
  getStudentsSearchByLastNameNoInscritByClass(id:number,page:number,lastName:String): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/search/students/notinscri/class/'+id+'/page?page='+page+'&'+'name='+lastName, { observe: 'response' })     
  }
  getAllStudentsNoInscrit(id:number,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/students/notinscri/class/'+id+'/page?page='+page, { observe: 'response' })     
  }
  findProfByClassId(id: number,page:number): Observable<EntityResponseType> {
    return this.httpClient.get<User>(this.UrlBase+'/profs/inscri/class/'+ id + '/page/?page='+page, { observe: 'response' })     
  }
  getProfsSearchByLastNameNoInscritByClass(id:number,page:number,lastName:String): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/search/profs/notinscri/class/'+id+'/page?page='+page+'&'+'name='+lastName, { observe: 'response' })     
  }
  getAllProfsNoInscrit(id:number,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/profs/notinscri/class/'+id+'/page?page='+page, { observe: 'response' })     
  }
  findProfBySubjectId(id: number,page:number): Observable<EntityResponseType> {
    return this.httpClient.get<User>(this.UrlBase+'/profs/subject/'+ id + '/page/?page='+page, { observe: 'response' })     
  }
  getAllProfsNoInscritBySubject(id:number,page:number): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/profs/notinscri/subject/'+id+'/page?page='+page, { observe: 'response' })     
  }
  getProfsSearchByLastNameNoInscritBySubject(id:number,page:number,lastName:String): Observable<HttpResponse<User[]>> {
    return this.httpClient.get<User[]>(this.UrlBase+'/search/profs/notinscri/subject/'+id+'/page?page='+page+'&'+'name='+lastName, { observe: 'response' })     
  }
  getEtudiantNumber(idClas:number,req?: any): Observable<any> {
    return this.httpClient.get<any>(this.UrlBase+'/students/'+idClas+'/class');
  }
  constructor(private httpClient: HttpClient,private router: Router) { 
    
  }
}
