import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { SubjectService } from '../services/subject.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { User } from '../models/user';
import { RankEtudiant } from '../models/RankEtudiant.model';
import { Clas } from '../models/class.model';
import { EtudiantSubjectNote } from '../models/EtudiantSubjectNote.model';
import { FormControl } from '@angular/forms';
import { Subject } from '../models/subject.model';
import { Idea } from '../models/idea.model';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { IdeaService } from '../services/idea.service';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  user: User
  userid : number
  userLastName:String
  userFirstName:String
  userEmail:String
  userRole:String
  queryField: FormControl = new FormControl();
  isAdmin:boolean;
  type:string;
  clas:string;
  notes:EtudiantSubjectNote[];
  eventSubscriber: Subscription;
  dateSelected:any;
  phone:string;
  address:string;
  classes:Clas[]
  dateSelectedMonth:any;
  moyByMonth:number;
  isMonth:boolean;
  rank:number;
  selectedClass:Clas;
  ranksEtudiants:RankEtudiant[];
   // pager object
   pager: any = {};

   // paged items
   pagedItems: any[];
   // array of all items to be paged
   private allItems: any[];
   ideas:Idea[]
   ideasByUser:Idea[]
   ideasByUserId:any[]
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<{ title: string; note: string; icon: string }> = [];
  constructor(private authService: AuthService,
    private subjectService: SubjectService,
    private userService: UserService,private router: Router,
    private modalService: NgbModal,
    private ideaService: IdeaService,
    private route: ActivatedRoute) {
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
         user.then(data => {
           this.user = data;
           console.log("data"+data["id"])
         });
      }
    );
    this.findIdeaByUser(localStorage.getItem('userId'));
    this.isMonth = false;
    //this.registerChangeInLocations()
    this.currentPage = 0
    if (localStorage.getItem('userRole')== "ROLE_ADMIN"){
      this.isAdmin = true
    }
    
    this.userService.findByid(localStorage.getItem('userId')).subscribe( (value) => {
       this.user =  value["body"];
       this.userLastName = value["body"]["lastName"]
       this.userFirstName = value["body"]["firstName"]
       this.userEmail = value["body"]["email"]
       this.type = value["body"]["type"]
       this.userRole = value["body"]["roles"][0]["name"]
       this.phone = value["body"]["phone"]
       this.address = value["body"]["address"]
       this.selectedClass = value["body"]["clas"]
       if (this.type == "etudiant" && value["body"]["clas"]!=null){
         this.clas = value["body"]["clas"]["name"]
         this.loadAll()
         
       }
       else{
        this.loadAll()
       }
    })
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
  findIdeaByUser(idUser:any){
    this.ideaService.findIdeaByUser(idUser).subscribe( (value) => {
      this.ideasByUser = value["body"]["content"];
      this.ideasByUserId = []
      this.ideasByUser.forEach(element => {
        this.ideasByUserId.push(element.id); 
      });
      })
  }

  loadAll(){
    this.ideaService.query(0).subscribe( (value) => {
      this.ideas = value["body"]["content"];
      this.ideas.forEach(element => {
        element.createdAt = element.createdAt.split("/")[0]
      });
      this.totalElements=value["body"]["totalElements"]
      this.totalPages = value["body"]["totalPages"] 
      if (this.totalPages>6){
        this.pageView =  6
      }else{
        this.pageView =  this.totalPages
      } 
    })
  }
}
