import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Subject } from 'src/app/models/subject.model';
import { SubjectService } from 'src/app/services/subject.service';
import { UserService } from 'src/app/services/user.service';
//import { MessageService } from 'src/app/services/message.service';
import { PagerService } from  'src/app/services/pager.service';
import { SearchService } from  'src/app/services/search.service'; 
import { Subscription } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage implements OnInit {
  totalItems: any;
  queryCount: any;
  page: any;
  subjects:Subject[]
  totalElements:number;
  totalPages:number;
  currentPage:number;
  pageView:number
  queryField: FormControl = new FormControl();
  compTitle:number = 1
  compDescription:number = 1
  subscription: Subscription;
  message: any;
  sortTitle:boolean = true;
  sortDesc:boolean = true;
  userRole:any;
  user: User;
  constructor(private menu: MenuController, private authService: AuthService, 
    private activatedRoute: ActivatedRoute,
    private _searchService: SearchService,private modalService: NgbModal, 
    private subjectService: SubjectService ,private userService: UserService,
    private route: ActivatedRoute ) { 
    this.menu.enable(true);
  }
  ngOnInit() {
    
  }
  ionViewWillEnter() {
    /*this.authService.user().subscribe(
      user => {
        this.user = user;
      }
    );*/
    this.authService.getUser().subscribe(
      user => {
         user.then(data => {
           this.user = data;
           this.getAll();
           console.log("data"+data["id"])
         });
      }
    );
  }

  getAll(){
    this.currentPage = 0
    this.subjectService.findSubjectsByEudiantId(this.user["id"],0).subscribe(  
      (value) => {
        if (value)
        {
          this.subjects= value["body"]["content"];
          console.log(value["body"].length +"this.classes"+this.subjects)
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.totalPages>6){
            this.pageView =  6
          }else{
            this.pageView =  this.totalPages
          }
        }
        //this.setPage(1);
      }
    );   
  }
  
  loadData(event?:any) {
    this.currentPage = this.subjects.length % 10;
      
    setTimeout(() => {
      this.subjectService.query(this.currentPage-1).subscribe(  
        (value) => { 
          this.subjects.concat(value["body"]["content"]); 
          this.totalElements=value["body"]["totalElements"]
          this.totalPages = value["body"]["totalPages"]
          if (this.currentPage-1 === this.totalPages) {
            //infiniteScrollEvent.enable(false);
            event.target.complete();

            // Disable the infinite scroll
            event.target.disabled = true;
          }
      }
    );
    }, 10);
  }
}